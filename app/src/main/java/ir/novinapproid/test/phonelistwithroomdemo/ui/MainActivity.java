package ir.novinapproid.test.phonelistwithroomdemo.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.ByteArrayOutputStream;
import java.util.List;

import ir.novinapproid.test.phonelistwithroomdemo.R;
import ir.novinapproid.test.phonelistwithroomdemo.adapters.ContactListAdapter;
import ir.novinapproid.test.phonelistwithroomdemo.db.sync.ContactOperationIntentService;
import ir.novinapproid.test.phonelistwithroomdemo.db.sync.DbTransactionThread;
import ir.novinapproid.test.phonelistwithroomdemo.events.AllContactsRetreivedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.ImgBitmapRetreivedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.NewContactAddedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.UserInsertedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.UserSelectedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.UserUpdatedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;
import ir.novinapproid.test.phonelistwithroomdemo.utils.Constants;
import ir.novinapproid.test.phonelistwithroomdemo.utils.sync.ImgOperationIntentService;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ContactListAdapter.OnContactListEdtBtnClickListener  {

    public static final String TAG = MainActivity.class.getSimpleName();

    public static final int PERMISSION_WRITE_READ_CONTACTS_REQUEST_CODE = 100;
    public static final int PICK_IMAGE_REQUEST_CODE = 200;

    private List<UserInfo> userInfos;
    private RecyclerView rv_contacts;
    private ContactListAdapter contactListAdapter;
    private AppCompatButton btn_add_contact;

    private AppCompatTextView tv_name;
    private AppCompatTextView tv_phone;
    private AppCompatTextView tv_adrs;
    private AppCompatTextView tv_job;

    private AppCompatImageView iv_pic;
    private AppCompatButton btn_select_pic;
    private AppCompatEditText edt_name;
    private AppCompatEditText edt_adrs;
    private AppCompatEditText edt_job;
    private AppCompatEditText edt_phone;
    private AppCompatButton btn_confirm;

    private Dialog dialog;

    private int position;
    private Bitmap bitmap;
    private UserInfo userInfo;
    private boolean insertUser;
    private String phone;
    private int userId;

    private Uri imgUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeView();

        boolean readWriteContactPermissionGranted = verifyReadWriteContactPermission();
        if (readWriteContactPermissionGranted) {

            executeContactOperation(Constants.GET_CONTACT_LIST);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult");


        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            imgUri = data.getData();
            executeImgOperation(Constants.GET_BITMAP);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_WRITE_READ_CONTACTS_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    // permission was denied, show alert to explain permission
                    showPermissionAlert();
                } else {
                    //permission is granted now start a background service
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

                        //do
                        executeContactOperation(Constants.GET_CONTACT_LIST);
                    }
                }
            }
        }
    }



    public void initializeView() {

        rv_contacts = findViewById(R.id.rv_contacts);
        rv_contacts.setLayoutManager(new LinearLayoutManager(this));
        rv_contacts.setHasFixedSize(true);
        btn_add_contact = findViewById(R.id.btn_add_contact);
        btn_add_contact.setOnClickListener(this);
    }

    //background operations
    public void executeContactOperation(int which) {
        Intent intent = new Intent(this, ContactOperationIntentService.class);

        switch (which){

            case Constants.GET_CONTACT_LIST:
                intent.putExtra(Constants.WHICH_CONTACT_OPERATION,Constants.GET_CONTACT_LIST);
                break;
            case Constants.ADD_NEW_CONTACT:
                intent.putExtra(Constants.WHICH_CONTACT_OPERATION,Constants.ADD_NEW_CONTACT);
                intent.putExtra(Constants.USER_PHONE,phone);
                break;
        }

        startService(intent);
    }

    public void executeImgOperation(int which) {
        Intent intent = new Intent(this, ImgOperationIntentService.class);

        switch (which){

            case Constants.GET_BITMAP:
                intent.putExtra(Constants.WHICH_IMG_OPERATION,Constants.GET_BITMAP);
                intent.putExtra(Constants.IMG_URI,imgUri);
                break;
        }

        startService(intent);
    }

    public void executeDbTransaction(int which) {

        switch (which){

            case Constants.INSERT_USER:
                new DbTransactionThread(this,Constants.INSERT_USER,userInfo);
                break;
            case Constants.GET_USER_BY_PHONE_FOR_EDIT_DIALOG:
                new DbTransactionThread(this,Constants.GET_USER_BY_PHONE_FOR_EDIT_DIALOG,userInfo);
                break;
            case Constants.GET_USER_BY_PHONE_FOR_DISPLAY_DIALOG:
                new DbTransactionThread(this,Constants.GET_USER_BY_PHONE_FOR_DISPLAY_DIALOG,userInfo);
                break;
            case Constants.UPDATE_USER:
                new DbTransactionThread(this,Constants.UPDATE_USER,userInfo);
                break;
        }

    }


    //permission operation
    public boolean verifyReadWriteContactPermission() {

        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_CONTACTS)
                    == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS)
                            == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_CONTACTS,
                                Manifest.permission.READ_CONTACTS}, PERMISSION_WRITE_READ_CONTACTS_REQUEST_CODE);
                return false;
            }
        } else {
            Log.v(TAG, "Permission is granted1");
            return true;
        }
    }

    private void showPermissionAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.permission_request_title);
        builder.setMessage(R.string.app_permission_notice);
        builder.create();
        builder.setPositiveButton(getString(R.string.confirm_str), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS}, PERMISSION_WRITE_READ_CONTACTS_REQUEST_CODE);
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel_str), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, R.string.permission_refused, Toast.LENGTH_LONG).show();
            }
        });
        builder.show();
    }


    //click handlers
    @Override
    public void onClick(View v) {

        Log.i(TAG, "btn_add_contact");
        showAddUserInfoDialog(Constants.ADD_USER_DIALOG);
    }

    @Override
    public void onContactListEdtBtnClick(int position) {

        Log.i(TAG, "onContactListEdtBtnClick");
        this.position = position;
        userInfo = contactListAdapter.getUserInfos().get(position);
        showAddUserInfoDialog(Constants.EDIT_USER_DIALOG);

    }

    @Override
    public void onContactListItemClick(int position) {

        Log.i(TAG, "onContactListItemClick");
        this.position = position;
        userInfo = contactListAdapter.getUserInfos().get(position);
        showDisplayUserInfoDialog();
    }



    //dialog operation
    public void showAddUserInfoDialog(final int whichDialog) {

        configureAddUserInfoDialog();


        btn_select_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "btn_select_pic");
                picImg();
            }
        });


        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "btn_confirm");

                if (whichDialog == Constants.ADD_USER_DIALOG) {

                     String name = edt_name.getText().toString();
                     phone = edt_phone.getText().toString();

                    userInfo = getUserInfoDataFromUi();
                    executeDbTransaction(Constants.INSERT_USER);
                    executeContactOperation(Constants.ADD_NEW_CONTACT);

                } else if (whichDialog == Constants.EDIT_USER_DIALOG) {

                    userInfo = getUserInfoDataFromUi();

                    if (insertUser) {
                        Log.i(TAG, " insertUserInfo()");

                        executeDbTransaction(Constants.INSERT_USER);

                    } else {

                        Log.i(TAG, "  updateUserInfo()");
                        userInfo.setId(userId);
                        executeDbTransaction(Constants.UPDATE_USER);
                    }
                }

                dialog.dismiss();


            }
        });

        dialog.show();

        if (whichDialog == Constants.EDIT_USER_DIALOG) {

            Log.i(TAG, " EDIT_USER_DIALOG");
            executeDbTransaction(Constants.GET_USER_BY_PHONE_FOR_EDIT_DIALOG);
        }

    }

    private void showDisplayUserInfoDialog() {

        configureDisplayUserInfoDialog();
        executeDbTransaction(Constants.GET_USER_BY_PHONE_FOR_DISPLAY_DIALOG);


    }


    public void configureAddUserInfoDialog() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_contact_to_db_dialog_lay);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        iv_pic = dialog.findViewById(R.id.iv_pic);
        btn_select_pic = dialog.findViewById(R.id.btn_select_pic);

        edt_name = dialog.findViewById(R.id.edt_name);
        edt_adrs = dialog.findViewById(R.id.edt_adrs);
        edt_job = dialog.findViewById(R.id.edt_job);
        edt_phone = dialog.findViewById(R.id.edt_phone);
        btn_confirm = dialog.findViewById(R.id.btn_confirm);
    }

    public void configureDisplayUserInfoDialog() {

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.user_info_dialog_lay);


        iv_pic = dialog.findViewById(R.id.iv_pic);
        tv_name = dialog.findViewById(R.id.tv_name);

        tv_phone = dialog.findViewById(R.id.tv_phone);
        tv_adrs = dialog.findViewById(R.id.tv_adrs);
        tv_job = dialog.findViewById(R.id.tv_job);

        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.show();
    }


    public void picImg() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE);
    }


    //all subscribes
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAllContactsRetreivedEvent(AllContactsRetreivedEvent allContactsRetreivedEvent) {

        List<UserInfo> userInfos = allContactsRetreivedEvent.getUserInfo();
        contactListAdapter = new ContactListAdapter(this, userInfos);
        contactListAdapter.setOnContactListEdtBtnClickListener(this);
        rv_contacts.setAdapter(contactListAdapter);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onImgBitmapRetreivedEvent(ImgBitmapRetreivedEvent imgBitmapRetreivedEvent) {

        bitmap = imgBitmapRetreivedEvent.getImgBitmap();
        iv_pic.setImageBitmap(bitmap);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserInsertedEvent(UserInsertedEvent userInsertedEvent) {

        String msg = getString(R.string.user_added_msg_str);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserUpdatedEvent(UserUpdatedEvent userUpdatedEvent) {

        String msg = getString(R.string.user_updated_msg_str);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNewContactAddedEvent(NewContactAddedEvent newContactAddedEvent) {

        Log.i(TAG,"onNewContactAddedEvent()");

        UserInfo userInfo = newContactAddedEvent.getUserInfo();
        List<UserInfo> userInfos = contactListAdapter.getUserInfos();
        userInfos.add(userInfo);
        contactListAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUserSelectedEvent(UserSelectedEvent userSelectedEvent) {

        Log.i(TAG,"onUserSelectedEvent()");

        int dataFroWhichDialog = userSelectedEvent.getDataFroWhichDialog();
        UserInfo u = userSelectedEvent.getUserInfo();

        if (dataFroWhichDialog == Constants.EDIT_USER_DIALOG){


            if (u == null){

                insertUser = true;
                edt_phone.setText(userInfo.getPhone());

            }else {

                userInfo = u;
                insertUser = false;
                setDataInEditDialog(u);
            }


        }else if (dataFroWhichDialog == Constants.DISPLAY_USER_DIALOG){

            Log.i(TAG,"DISPLAY_USER_DIALOG");
            if (u == null){

                tv_phone.setText(userInfo.getPhone());

            }else {
                userInfo = u;
                setDataInDisplayDialog(u);
            }
        }

    }


    //ui operation
    public UserInfo getUserInfoDataFromUi() {

        String adrs = edt_adrs.getText().toString();
        String job = edt_job.getText().toString();
        String name = edt_name.getText().toString();
        String phone = edt_phone.getText().toString();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bArray = bos.toByteArray();

        UserInfo u = new UserInfo(name, adrs, job, phone);
        u.setImage(bArray);

        return u;
    }

    public void setDataInEditDialog(UserInfo userInfo) {

            userId = userInfo.getId();
            String name = userInfo.getName();
            String adrs = userInfo.getAdrs();
            String job = userInfo.getJob();
            String phone = userInfo.getPhone();

            edt_phone.setText(phone);
            edt_name.setText(name);
            edt_job.setText(job);
            edt_adrs.setText(adrs);

            setByteArrToImgView();

    }

    public void setDataInDisplayDialog(UserInfo userInfo) {
        Log.i(TAG,"setDataInDisplayDialog()");
        userId = userInfo.getId();
        String name = userInfo.getName();
        String adrs = userInfo.getAdrs();
        String job = userInfo.getJob();
        String phone = userInfo.getPhone();

        tv_phone.setText(phone);
        tv_name.setText(name);
        tv_job.setText(job);
        tv_adrs.setText(adrs);

        setByteArrToImgView();

    }

    public  void  setByteArrToImgView(){

        byte[] image = userInfo.getImage();

        bitmap = BitmapFactory.decodeByteArray(image, 0, image.length);
        iv_pic.setImageBitmap(Bitmap.createScaledBitmap(bitmap, iv_pic.getWidth(),
                iv_pic.getHeight(), false));
    }
}
