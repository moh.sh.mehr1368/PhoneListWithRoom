package ir.novinapproid.test.phonelistwithroomdemo.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ir.novinapproid.test.phonelistwithroomdemo.R;
import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;

/**
 * Created by mohammadreza on 7/2/2018.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactViewHolder> {
    private Context mContext;
    private List<UserInfo> userInfos;
    private LayoutInflater mLayoutInflater;

    public ContactListAdapter(Context mContext, List<UserInfo> userInfos) {
        this.mContext = mContext;
        this.userInfos = userInfos;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.contact_list_item_lay,parent,false);
        ContactViewHolder holder = new ContactViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {

        String phone = userInfos.get(position).getPhone();
        holder.tv_phone.setText(phone);

    }

    @Override
    public int getItemCount() {
        return userInfos.size();
    }

    static class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        AppCompatTextView tv_phone;
        AppCompatImageView iv_edit;
        public ContactViewHolder(View itemView) {
            super(itemView);
            tv_phone = itemView.findViewById(R.id.tv_phone);
            iv_edit = itemView.findViewById(R.id.iv_edit);
            iv_edit.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.iv_edit) {
                if (onContactListEdtBtnClickListener != null) {

                    onContactListEdtBtnClickListener.onContactListEdtBtnClick(getAdapterPosition());
                }
            }else if (id == R.id.ll_root){

                if (onContactListEdtBtnClickListener != null) {

                    onContactListEdtBtnClickListener.onContactListItemClick(getAdapterPosition());
                }
            }
        }
    }

    public interface  OnContactListEdtBtnClickListener{
        public void onContactListEdtBtnClick(int position);
        public void onContactListItemClick(int position);
    }

    static OnContactListEdtBtnClickListener onContactListEdtBtnClickListener;

    public void setOnContactListEdtBtnClickListener(OnContactListEdtBtnClickListener onContactListEdtBtnClickListener){
        this.onContactListEdtBtnClickListener = onContactListEdtBtnClickListener;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }
}
