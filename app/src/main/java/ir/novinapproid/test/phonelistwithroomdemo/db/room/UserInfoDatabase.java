package ir.novinapproid.test.phonelistwithroomdemo.db.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;

/**
 * Created by mohammadreza on 7/15/2018.
 */

@Database(entities = {UserInfo.class},version = 1, exportSchema = false)
public abstract class UserInfoDatabase extends RoomDatabase {

    public abstract UserInfoDao userInfoDao();
}
