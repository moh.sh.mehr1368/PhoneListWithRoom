package ir.novinapproid.test.phonelistwithroomdemo.events;

import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class UserSelectedEvent {

    private UserInfo userInfo;
    private int dataFroWhichDialog;

    public UserSelectedEvent(UserInfo userInfo,int dataFroWhichDialog) {
        this.userInfo = userInfo;
        this.dataFroWhichDialog = dataFroWhichDialog;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public int getDataFroWhichDialog() {
        return dataFroWhichDialog;
    }
}
