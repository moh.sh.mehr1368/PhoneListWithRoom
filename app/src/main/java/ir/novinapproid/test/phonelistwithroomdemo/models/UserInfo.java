package ir.novinapproid.test.phonelistwithroomdemo.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class UserInfo{

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name" )
    private String name;
    @ColumnInfo(name = "adrs" )
    private String adrs;
    @ColumnInfo(name = "job" )
    private String job;
    @ColumnInfo(name = "phone" )
    private String phone;
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    public UserInfo() {
    }

    public UserInfo(String name, String adrs, String job, String phone) {
        this.name = name;
        this.adrs = adrs;
        this.job = job;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdrs() {
        return adrs;
    }

    public void setAdrs(String adrs) {
        this.adrs = adrs;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
