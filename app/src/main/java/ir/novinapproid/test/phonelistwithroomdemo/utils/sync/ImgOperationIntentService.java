package ir.novinapproid.test.phonelistwithroomdemo.utils.sync;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import ir.novinapproid.test.phonelistwithroomdemo.events.ImgBitmapRetreivedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.utils.Constants;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class ImgOperationIntentService extends IntentService {

    private Intent intent;

    public ImgOperationIntentService() {
        super("ImgOperationIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        this.intent = intent;
        int whichContactOperation = intent.getIntExtra(Constants.WHICH_IMG_OPERATION,0);
        executeOperation(whichContactOperation);
    }

    public void executeOperation(int which) {

        switch (which) {

            case Constants.GET_BITMAP:
                getBitmap();
                break;
        }
    }

    public void getBitmap() {
        Uri imgUri = intent.getParcelableExtra(Constants.IMG_URI);
        try {
            Bitmap imgBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
            EventBus.getDefault().post(new ImgBitmapRetreivedEvent(imgBitmap));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
