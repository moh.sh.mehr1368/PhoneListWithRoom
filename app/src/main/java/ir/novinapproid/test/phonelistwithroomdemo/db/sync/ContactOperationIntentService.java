package ir.novinapproid.test.phonelistwithroomdemo.db.sync;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import ir.novinapproid.test.phonelistwithroomdemo.events.AllContactsRetreivedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.NewContactAddedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;
import ir.novinapproid.test.phonelistwithroomdemo.utils.Constants;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class ContactOperationIntentService extends IntentService {

    private static final String TAG = ContactOperationIntentService.class.getSimpleName();

    private Intent intent;

    public ContactOperationIntentService() {
        super("ContactOperationIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        this.intent = intent;
        int whichContactOperation = intent.getIntExtra(Constants.WHICH_CONTACT_OPERATION,0);
        executeTransaction(whichContactOperation);
    }


    public void executeTransaction(int which) {

        switch (which) {

            case Constants.GET_CONTACT_LIST:
                getContactList();
                break;
            case Constants.ADD_NEW_CONTACT:
                addContact();
                break;
        }
    }


    private void getContactList() {

        List<UserInfo> userInfos = new ArrayList<>();

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);


                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Log.i(TAG, "Name: " + name);
                        Log.i(TAG, "Phone Number: " + phoneNo);


                        UserInfo userInfo = new UserInfo();
                        userInfo.setPhone(phoneNo);
                        userInfos.add(userInfo);
                    }

                    EventBus.getDefault().post(new AllContactsRetreivedEvent(userInfos));
                    pCur.close();
                }
            }
        }
        if (cur != null) {
            cur.close();
        }
    }


    private void addContact() {

        String phone = intent.getStringExtra(Constants.USER_PHONE);


        Uri addContactsUri = ContactsContract.Data.CONTENT_URI;

        long rowContactId = getRawContactId();

        int typeMobile = ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;

        String provider = "com.android.providers.contacts";

        // grant all three uri permissions!
        grantUriPermission(provider, addContactsUri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        grantUriPermission(provider, addContactsUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        grantUriPermission(provider, addContactsUri, Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);


        insertContactPhoneNumber(addContactsUri, rowContactId, phone, typeMobile);
    }

    private long getRawContactId()
    {
        ContentValues contentValues = new ContentValues();
        Uri rawContactUri = getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues);
        long ret = ContentUris.parseId(rawContactUri);
        return ret;
    }

    private void insertContactPhoneNumber(Uri addContactsUri, long rawContactId, String phoneNumber, int phoneContactType)
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);

        contentValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);

        contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber);


        contentValues.put(ContactsContract.CommonDataKinds.Phone.TYPE, phoneContactType);

        getContentResolver().insert(addContactsUri, contentValues);
        UserInfo userInfo = new UserInfo();
        userInfo.setPhone(phoneNumber);
        EventBus.getDefault().post(new NewContactAddedEvent(userInfo));
    }
}
