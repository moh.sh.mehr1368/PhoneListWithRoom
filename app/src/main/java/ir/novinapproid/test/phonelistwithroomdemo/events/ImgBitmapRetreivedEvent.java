package ir.novinapproid.test.phonelistwithroomdemo.events;

import android.graphics.Bitmap;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class ImgBitmapRetreivedEvent {

    private Bitmap imgBitmap;

    public ImgBitmapRetreivedEvent(Bitmap imgBitmap) {
        this.imgBitmap = imgBitmap;
    }

    public Bitmap getImgBitmap() {
        return imgBitmap;
    }
}
