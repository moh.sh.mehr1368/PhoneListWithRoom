package ir.novinapproid.test.phonelistwithroomdemo.utils;

public class Constants {

    public static final int ADD_USER_DIALOG = 154;
    public static final int EDIT_USER_DIALOG = 164;
    public static final int DISPLAY_USER_DIALOG = 165;

    public static final int GET_USER_BY_PHONE_FOR_EDIT_DIALOG = 174;
    public static final int GET_USER_BY_PHONE_FOR_DISPLAY_DIALOG = 175;
    public static final int INSERT_USER = 184;
    public static final int UPDATE_USER = 194;
    public static final int GET_ALL_USER = 204;

    public static final int GET_CONTACT_LIST = 224;
    public static final int ADD_NEW_CONTACT = 234;

    public static final String WHICH_CONTACT_OPERATION = "which_contact_operation";

    public static final String WHICH_IMG_OPERATION = "which_img_operation";
    public static final String IMG_URI = "img_uri";

    public static final String USER_PHONE = "user_phone";

    public static final int GET_BITMAP = 244;

}
