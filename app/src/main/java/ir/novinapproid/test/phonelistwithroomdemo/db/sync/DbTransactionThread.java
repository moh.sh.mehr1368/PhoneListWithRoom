package ir.novinapproid.test.phonelistwithroomdemo.db.sync;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import ir.novinapproid.test.phonelistwithroomdemo.db.DbQueries;
import ir.novinapproid.test.phonelistwithroomdemo.db.room.UserInfoDao;
import ir.novinapproid.test.phonelistwithroomdemo.db.room.UserInfoDatabase;
import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;
import ir.novinapproid.test.phonelistwithroomdemo.utils.Constants;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class DbTransactionThread {

    public static final String TAG = DbTransactionThread.class.getSimpleName();
    private Looper mLooper;
    private DbTransactionHandler dbTransactionHandler;
    private Context context;
    private int which;
    private UserInfo userInfo;
    private DbQueries dbQueries;

    private static UserInfoDatabase userInfoDatabase;

    private UserInfoDao userInfoDao;


    public DbTransactionThread(Context context, int which, UserInfo userInfo) {

        this.context = context;
        this.which = which;
        this.userInfo = userInfo;
        HandlerThread thread = new HandlerThread("DbTransactionThread", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        mLooper = thread.getLooper();
        dbTransactionHandler = new DbTransactionHandler(mLooper);
        Message msg = dbTransactionHandler.obtainMessage();
        dbTransactionHandler.sendMessage(msg);
    }

    private final class DbTransactionHandler extends Handler {
        public DbTransactionHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            getUserInfoDatabase(context);
            userInfoDao = userInfoDatabase.userInfoDao();
            dbQueries = new DbQueries(userInfoDao);
            dbQueries.setUserInfo(userInfo);
            executeTransaction(which);
        }
    }


    public static synchronized UserInfoDatabase getUserInfoDatabase(Context context) {
        if (userInfoDatabase == null) {
            configureUserInfoDatabase(context);
        }
        return userInfoDatabase;
    }

    public static void configureUserInfoDatabase(Context context) {

        userInfoDatabase = Room
                .databaseBuilder(context, UserInfoDatabase.class, "user_info")
                .fallbackToDestructiveMigration()
                .build();
    }

    public void executeTransaction(int which) {

        switch (which) {

            case Constants.GET_ALL_USER:
                dbQueries.selectAllUsers();
                break;
            case Constants.GET_USER_BY_PHONE_FOR_EDIT_DIALOG:
                dbQueries.selectUserByPhone(which);
                break;
            case Constants.GET_USER_BY_PHONE_FOR_DISPLAY_DIALOG:
                dbQueries.selectUserByPhone(which);
                break;
            case Constants.INSERT_USER:
                dbQueries.insertUser();
                break;
            case Constants.UPDATE_USER:
                dbQueries.updateUserById();
                break;
        }
    }


}
