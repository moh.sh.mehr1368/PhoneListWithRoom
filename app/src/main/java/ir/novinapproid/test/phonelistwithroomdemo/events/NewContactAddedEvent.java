package ir.novinapproid.test.phonelistwithroomdemo.events;

import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class NewContactAddedEvent {

    private UserInfo userInfo;

    public NewContactAddedEvent(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
