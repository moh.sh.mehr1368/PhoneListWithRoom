package ir.novinapproid.test.phonelistwithroomdemo.db.room;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;

@Dao
public interface UserInfoDao {

    @Query("SELECT * FROM UserInfo")
    List<UserInfo> getAll();


    @Query("SELECT * FROM UserInfo WHERE phone LIKE :phone")
    public UserInfo getUserInfoByPhone(String phone);


    @Query("UPDATE UserInfo SET  name = :name , adrs = :adrs, job = :job, phone = :phone, image = :image WHERE id = :id")
    public void updateUserById(int id, String name, String adrs, String job, String phone, byte[] image);


    @Insert
    void insertAll(UserInfo... userInfos);

}
