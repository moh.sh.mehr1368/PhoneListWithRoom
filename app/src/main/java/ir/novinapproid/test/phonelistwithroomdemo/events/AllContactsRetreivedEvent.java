package ir.novinapproid.test.phonelistwithroomdemo.events;

import java.util.List;

import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class AllContactsRetreivedEvent {

    private List<UserInfo> userInfos;

    public AllContactsRetreivedEvent(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public List<UserInfo> getUserInfo() {
        return userInfos;
    }
}
