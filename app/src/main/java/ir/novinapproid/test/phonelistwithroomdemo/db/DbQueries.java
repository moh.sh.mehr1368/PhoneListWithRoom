package ir.novinapproid.test.phonelistwithroomdemo.db;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import ir.novinapproid.test.phonelistwithroomdemo.db.room.UserInfoDao;
import ir.novinapproid.test.phonelistwithroomdemo.events.UserInsertedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.UserSelectedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.events.UserUpdatedEvent;
import ir.novinapproid.test.phonelistwithroomdemo.models.UserInfo;
import ir.novinapproid.test.phonelistwithroomdemo.utils.Constants;

/**
 * Created by mohammadreza on 7/18/2018.
 */

public class DbQueries {

    private static final String TAG = DbQueries.class.getSimpleName();
    private UserInfoDao userInfoDao;
    private UserInfo userInfo;

    public DbQueries(UserInfoDao userInfoDao) {
        this.userInfoDao = userInfoDao;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public void selectUserByPhone(int which){

        Log.i(TAG, "getUserByPhone()");
        String phone = userInfo.getPhone();
        UserInfo userInfoByPhone = userInfoDao.getUserInfoByPhone(phone);

        if (which == Constants.GET_USER_BY_PHONE_FOR_EDIT_DIALOG){

            EventBus.getDefault().post(new UserSelectedEvent(userInfoByPhone,Constants.EDIT_USER_DIALOG));

        }else if (which == Constants.GET_USER_BY_PHONE_FOR_DISPLAY_DIALOG){

            EventBus.getDefault().post(new UserSelectedEvent(userInfoByPhone,Constants.DISPLAY_USER_DIALOG));
        }

    }

    public void updateUserById(){

        Log.i(TAG, "updateUserById()");
        int userId = userInfo.getId();
        String adrs = userInfo.getAdrs();
        byte[] image = userInfo.getImage();
        String job = userInfo.getJob();
        String name = userInfo.getName();
        String phone = userInfo.getPhone();
        userInfoDao.updateUserById(userId,name,adrs,job,phone,image);
        EventBus.getDefault().post(new UserUpdatedEvent());

    }

    public void insertUser(){

        Log.i(TAG, "insertUser()");
        userInfoDao.insertAll(userInfo);
        EventBus.getDefault().post(new UserInsertedEvent());

    }

    public void selectAllUsers(){

    }
}
